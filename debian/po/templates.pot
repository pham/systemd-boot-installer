# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the systemd-boot-installer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: systemd-boot-installer\n"
"Report-Msgid-Bugs-To: systemd-boot-installer@packages.debian.org\n"
"POT-Creation-Date: 2024-06-02 22:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl3:
#: ../systemd-boot-installer.templates:1001
msgid "Install the systemd-boot boot loader"
msgstr ""
